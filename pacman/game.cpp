

#include <iostream>
#include <Windows.h>
#include <conio.h>
#include <ctime>
#include <list>

using namespace std;
// 8 = walls
//1 = small pills
// 2 = power pills
//5 = pacman right
//6 = pacman left
//7 = pacman up
//8 = pacman down
// 87 = ghost1
// 88 = ghost2
// 89 = ghost3

const int MAXROWS = 32;
const int MAXCOLUMNS = 28;

LARGE_INTEGER frequency;        // ticks per second
LARGE_INTEGER t1, t2;           // ticks
double elapsedTime;
double totalElapsedTime = 0;
int countElapsedTime = 0;
double averageElapesdTime = 0;

char keyboardValue = 127;
//Start position is [15][0]
int pacI = 15, pacJ = 0;
//Ghost1 Start postion is [16][14]
int ghostOneI = 16, ghostOneJ = 14;
int node1i = 16, node1j = 5;
int i = 0, j = 0;
char key = 127;

char levelMatrix[MAXROWS][MAXCOLUMNS] =
{
	{ 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 },
	{ 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8 },
	{ 8, 1,  8,  8,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8,  8,  8, 1,  8 },
	{ 8,  2,  8,  8,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8,  8,  8,  2,  8 },
	{ 8, 1,  8,  8,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8,  8,  8, 1,  8 },
	{ 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  8 },
	{ 8, 1,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8, 1,  8 },
	{ 8, 1,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8, 1,  8 },
	{ 8, 1, 1, 1, 1, 1, 1,  8,  8, 1, 1, 1, 1,  8,  8, 1, 1, 1, 1,  8,  8, 1, 1, 1, 1, 1, 1,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8,  8,  8,  8, 0,  8,  8, 0,  8,  8,  8,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8,  8,  8,  8, 0,  8,  8, 0,  8,  8,  8,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8, 0,  8,  8, 0, 0, 0, 0,  8,  8, 0,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8, 0,  8, 0, 0, 0, 0, 0, 0,  8, 0,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8, 0,  8, 0, 0, 0, 0, 0, 0,  8, 0,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 5, 0, 0, 0, 0, 0, 1, 0, 0, 0,  8, 0, 0, 0, 0, 0, 0,  8, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8, 0,  8, 0, 0, 0, 0, 0, 0,  8, 0,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8, 0,  8, 0, 0, 0, 0, 0, 0,  8, 0,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8, 0,  8,  8, 0, 0, 0, 0,  8,  8, 0,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8,  8,  8,  8,  8,  8, 1,  8,  8, 0,  8,  8,  8,  8,  8,  8,  8,  8, 0,  8,  8, 1,  8,  8,  8,  8,  8,  8 },
	{ 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  8,  8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  8 },
	{ 8, 1,  8,  8,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8,  8,  8, 1,  8 },
	{ 8, 1,  8,  8,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8, 1,  8,  8,  8,  8, 1,  8 },
	{ 8,  2, 1, 1,  8,  8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  8,  8, 1, 1,  2,  8 },
	{ 8,  8,  8, 1,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8, 1,  8,  8,  8 },
	{ 8,  8,  8, 1,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8, 1,  8,  8,  8 },
	{ 8, 1, 1, 1, 1, 1, 1,  8,  8, 1, 1, 1, 1,  8,  8, 1, 1, 1, 1,  8,  8, 1, 1, 1, 1, 1, 1,  8 },
	{ 8, 1,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8, 1,  8 },
	{ 8, 1,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8, 1,  8,  8, 1,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8, 1,  8 },
	{ 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  8 },
	{ 8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8 }
};

std::list<char> levelMatrixList;
std::list<list<list<char>>> grid;

void getKeyPress();
//bool blockValid(int block);
void ghost();
void goToXY(int column, int line);
bool ghostMoveUp();
bool ghostMoveDown();
bool ghostMoveLeft();
bool ghostMoveRight();
void setPillsCount();
//int findNode();
void setLevelMatrix(int x, int y, char value);
char getLevelMatrix(int x, int y);
void putLevelMatrixToList();
void setDataToList(int x, int y, char value);
char getDataFromList(int x, int y);

int pillCount = 244;
int lives = 5;
int score = 0;
clock_t startTime = 0, endTime = 0;
bool superPellet = false;
bool pill = false;

char ghostCurrent = getLevelMatrix(ghostOneI, ghostOneJ);
char ghostTrail = getLevelMatrix(ghostOneI, ghostOneJ);

int main()
{
	HANDLE  hConsole;
	int k = 14;

	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, k);


	cout << "\n          ########" << endl;
	cout << "       ###############" << endl;
	cout << "    ########  ###########" << endl;
	cout << "  #####################" << endl;
	cout << " ##################" << endl;
	cout << "################" << endl;
	cout << "#############" << endl;
	cout << " ###############" << endl;
	cout << "  #################" << endl;
	cout << "    ###################" << endl;
	cout << "       #############" << endl;
	cout << "\n       PACMAN GAME" << endl;

	cout << endl;
	system("pause");
	char key = 0;
	system("mode 80, 50");
	
	bool stopCounting = false;

	setPillsCount();

	// use link list version of game
	//putLevelMatrixToList();

	// get ticks per second
	QueryPerformanceFrequency(&frequency);

	//create the matrix 
	//28 columns
	//32 high

	do
	{

		//Set Start position
		goToXY(0, 0);
		ghost();
		//Column
		for (i = 0; i < MAXROWS; i++)
		{
			//Row
			for (j = 0; j < MAXCOLUMNS; j++)
			{
				//Walls
				if (getLevelMatrix(i, j) == 8)
				{
					k = 8;
					SetConsoleTextAttribute(hConsole, k);
					cout << char(219);
					cout << char(219);
				}
				//Pacman
				else if (getLevelMatrix(i, j) == 5 || getLevelMatrix(i, j) == 6 || getLevelMatrix(i, j) == 7 || getLevelMatrix(i, j) == 8)
				{
					k = 14;
					SetConsoleTextAttribute(hConsole, k);
					cout << char(220);
					cout << char(32);
				}
				//Blank
				else if (getLevelMatrix(i, j) == 0)
				{
					k = 0;
					SetConsoleTextAttribute(hConsole, k);
					cout << char(32);
					cout << char(32);
				}
				//Ghost
				else if (getLevelMatrix(i, j) == 87)
				{
					if (superPellet)
					{
						k = 4;
						SetConsoleTextAttribute(hConsole, k);
					}
					else
					{
						k = 12;
						SetConsoleTextAttribute(hConsole, k);
					}
					cout << char(220);
					cout << char(32);
				}
				//pill
				else if (getLevelMatrix(i, j) == 1)
				{
					k = 15;
					SetConsoleTextAttribute(hConsole, k);
					cout << char(250);
					cout << char(32);
				}
				//Power pill
				else if (getLevelMatrix(i, j) == 2)
				{
					k = 10;
					SetConsoleTextAttribute(hConsole, k);
					cout << char(149);
					cout << char(32);

				}

				if (j == 27)
					cout << endl;

				if (j == 27 && i == 31)
					stopCounting = true;
			}
		}

		// stop timer
		QueryPerformanceCounter(&t2);

		// compute and print the elapsed time in millisec
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		totalElapsedTime += elapsedTime;
		countElapsedTime++;
		
		// after 50 count calculate avarage value
		if (countElapsedTime > 50) 
		{
			averageElapesdTime = totalElapsedTime / countElapsedTime;
			countElapsedTime = 0;
			totalElapsedTime = 0;
		}

		getKeyPress();

		// start timer
		QueryPerformanceCounter(&t1);

		//Color
		k = 15;
		SetConsoleTextAttribute(hConsole, k);

		cout << "\n lives: " << lives;
		cout << "\t score: " << score << endl;


		endTime = clock();
		int elapsed = int((endTime - startTime) / CLOCKS_PER_SEC);
		//cout  << elapsed;

		if (elapsed > 5)
		{
			superPellet = false;
			elapsed = 0;
		}

		cout << "\r\nelapsed time: " << elapsedTime << "\t average: " << averageElapesdTime << endl;
		
	} while (lives > 0 && pillCount >= 0);

	// terminate the game
	system("pause");
	return 0;
};



bool ghostMoveUp()
{
	char ghostNextUp = getLevelMatrix(ghostOneI - 1, ghostOneJ);
	//Check the next upward block to be sure it is not a wall
	if (ghostNextUp != 8)
	{
		//Check the last block to see if there was a pill
		if (ghostNextUp == 1 || ghostNextUp == 117)
		{
			//put the pill back
			setLevelMatrix(ghostOneI - 1, ghostOneJ, 87);

			setLevelMatrix(ghostOneI, ghostOneJ, ghostTrail);
			ghostOneI--;
		}
		else
		{
			pill = false;
			setLevelMatrix(ghostOneI, ghostOneJ, 0);
			setLevelMatrix(ghostOneI - 1, ghostOneJ, 87);
			ghostOneI--;
		}
		return true;
	}
	else
	{
		return false;
	}
}

bool ghostMoveDown()
{

	char ghostNextDown = getLevelMatrix(ghostOneI + 1, ghostOneJ);
	if (ghostNextDown != 8)
	{
		if (ghostNextDown == 1 || ghostNextDown == 117)
		{
			setLevelMatrix(ghostOneI + 1, ghostOneJ, 87);
			setLevelMatrix(ghostOneI, ghostOneJ, ghostNextDown);
			ghostOneI++;
		}
		else
		{
			setLevelMatrix(ghostOneI, ghostOneJ, 0);
			setLevelMatrix(ghostOneI + 1, ghostOneJ, 87);
			ghostOneI++;
		}
		return true;
	}
	else
	{
		return false;
	}
}

bool ghostMoveLeft()
{
	char ghostNextLeft = getLevelMatrix(ghostOneI, ghostOneJ - 1);
	if (ghostNextLeft != 8)
	{
		if (ghostNextLeft == 1 || ghostNextLeft == 117)
		{
			setLevelMatrix(ghostOneI, ghostOneJ - 1, 87);
			setLevelMatrix(ghostOneI, ghostOneJ, ghostNextLeft);
			ghostOneJ--;
		}
		else
		{
			setLevelMatrix(ghostOneI, ghostOneJ, 0);
			setLevelMatrix(ghostOneI, ghostOneJ - 1, 87);
			ghostOneJ--;
		}
		return true;
	}
	else
	{
		return false;
	}

}

bool ghostMoveRight()
{
	char ghostNextRight = getLevelMatrix(ghostOneI, ghostOneJ + 1);
	if (ghostNextRight != 8)
	{

		if (ghostNextRight == 1 || ghostNextRight == 117)
		{
			setLevelMatrix(ghostOneI, ghostOneJ + 1, 87);
			setLevelMatrix(ghostOneI, ghostOneJ, ghostNextRight);
			ghostOneJ++;
		}
		else
		{
			setLevelMatrix(ghostOneI, ghostOneJ, 0);
			setLevelMatrix(ghostOneI, ghostOneJ + 1, 87);
			ghostOneJ++;
		}
		return true;
	}
	else
	{
		return false;
	}

}

void ghost()
{
	if (superPellet)
	{
		//Ghost is moving up 
		if (pacI > ghostOneI)
		{
			//if wall found
			if (!ghostMoveUp())
			{
				//find node path 
			}
		}

		//Ghost is moving down
		else if (pacI < ghostOneI)
		{
			if (!ghostMoveDown())
			{

			}
		}

		//Ghost is moving left
		if (pacJ > ghostOneJ)
		{
			if (!ghostMoveLeft())
			{

			}
		}

		//Ghost is moving right
		else if (pacJ < ghostOneJ)
		{
			if (!ghostMoveRight())
			{

			}
		}
	}
	else
	{


		//Ghost is moving up 
		if (pacI < ghostOneI)
		{

			//if wall found
			if (!ghostMoveUp())
			{

			}
		}

		//Ghost is moving down
		else if (pacI > ghostOneI)
		{
			if (!ghostMoveDown())
			{

			}
		}

		//Ghost is moving left
		if (pacJ < ghostOneJ)
		{
			if (!ghostMoveLeft())
			{

			}
		}

		//Ghost is moving right
		else if (pacJ > ghostOneJ)
		{
			if (!ghostMoveRight())
			{

			}
		}
	}

	//you caught the ghost
	if (pacJ == ghostOneJ && pacI == ghostOneI && superPellet)
	{
		setLevelMatrix(ghostOneI, ghostOneJ, 5);
		ghostOneI = 16;
		ghostOneJ = 14;
		setLevelMatrix(16, 14, 87);
	}
	//Reset the postion of pacman you died
	else if (pacJ == ghostOneJ && pacI == ghostOneI)
	{
		lives--;
		setLevelMatrix(pacI, pacJ, 0);
		setLevelMatrix(ghostOneI, ghostOneJ, 0);
		pacI = 15;
		pacJ = 0;
		ghostOneI = 16;
		ghostOneJ = 14;
		setLevelMatrix(15, 0, 5);
		setLevelMatrix(16, 14, 87);
	}
}

void getKeyPress()
{
	key = _getch();
	char pacTemp = '<';
	//get a special key
	if (key == 0 || key == -32)
	{
		key = _getch();

		if (key == 72) //up
		{
			if (getLevelMatrix(pacI - 1, pacJ) != 8)
			{
				if (getLevelMatrix(pacI - 1, pacJ) == 2)
				{
					score += 10;
					startTime = clock();
					superPellet = true;
				}
				else if (getLevelMatrix(pacI - 1, pacJ) == 1) {
					score++;
					pillCount--;
				}
				setLevelMatrix(pacI, pacJ, 0);
				setLevelMatrix(pacI - 1, pacJ, 7);
				pacI--;
			}
		}
		else if (key == 75) //left
		{
			if (getLevelMatrix(pacI, pacJ - 1) != 8)
			{
				if (getLevelMatrix(pacI, pacJ - 1) == 2)
				{
					score += 10;
					startTime = clock();
					superPellet = true;
				}
				else if (getLevelMatrix(pacI, pacJ - 1) == 1) {
					score++;
					pillCount--;
				}
				setLevelMatrix(pacI, pacJ, 0);
				setLevelMatrix(pacI, pacJ - 1, 6);
				pacJ--;
			}
		}
		else if (key == 77) //right
		{
			if (getLevelMatrix(pacI, pacJ + 1) != 8)
			{
				if (getLevelMatrix(pacI, pacJ + 1) == 2)
				{
					score += 10;
					startTime = clock();
					superPellet = true;
				}
				else if (getLevelMatrix(pacI, pacJ + 1) == 1) {
					score++;
					pillCount--;
				}
				setLevelMatrix(pacI, pacJ, 0);
				setLevelMatrix(pacI, pacJ + 1, 5);
				pacJ++;
			}
		}
		else if (key == 80) //down
		{
			if (getLevelMatrix(pacI + 1, pacJ) != 8)
			{
				if (getLevelMatrix(pacI + 1, pacJ) == 2)
				{
					score += 10;
					startTime = clock();
					superPellet = true;
				}
				else if (getLevelMatrix(pacI + 1, pacJ) == 1) {
					score++;
					pillCount--;
				}
				setLevelMatrix(pacI, pacJ, 0);
				setLevelMatrix(pacI + 1, pacJ, 5);
				pacI++;
			}
		}
	}
}

void setPillsCount() {
	
	int pills = 0;
	
	for (i = 0; i < MAXROWS; i++)
	{
		//Row
		for (j = 0; j < MAXCOLUMNS; j++)
		{
			//pills
			if (getLevelMatrix(i, j) == 1)
			{
				pills++;
			}
		}
	}

	pillCount = pills;
}

void goToXY(int column, int line)
{
	// Create a COORD structure and fill in its members.
	// This specifies the new position of the cursor that we will set.
	COORD coord;
	coord.X = column;
	coord.Y = line;
		
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		
	if (!SetConsoleCursorPosition(hConsole, coord))
	{
		
	}
}

void setLevelMatrix(int x, int y, char value)
{
	levelMatrix[x][y] = value;
	//setDataToList(x, y, value);
}

char getLevelMatrix(int x, int y) 
{
	return levelMatrix[x][y];
	//return getDataFromList(x, y);
}

// after putting data in to list then data are saved in liner way
void putLevelMatrixToList() 
{	
	for (int i = 0; i < MAXROWS; i++)
	{
		for (int j = 0; j < MAXCOLUMNS; j++) 
		{
			levelMatrixList.push_back(levelMatrix[i][j]);
			
		}
	}
}

void setDataToList(int x, int y, char value) 
{
	unsigned N = MAXCOLUMNS*x + y;	

	if (levelMatrixList.size() > N)
	{
		std::list<char>::iterator it = std::next(levelMatrixList.begin(), N);		
		*it = value;
	}
}

char getDataFromList(int x, int y) 
{
	unsigned N = MAXCOLUMNS*x + y;
	char value = ' ';

	if (levelMatrixList.size() > N)
	{
		std::list<char>::iterator it = std::next(levelMatrixList.begin(), N);
		// 'it' points to the element at index 'N'
		value = *it;
	}

	return value;
}
